# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "alertmanager/map.jinja" import alertmanager with context %}

alertmanager-remove-service:
  service.dead:
    - name: alertmanager
  file.absent:
    - name: /etc/systemd/system/alertmanager.service
    - require:
      - service: alertmanager-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: alertmanager-remove-service

alertmanager-remove-symlink:
   file.absent:
    - name: {{ alertmanager.bin_dir}}/alertmanager
    - require:
      - alertmanager-remove-service

#alertmanager-remove-binary:
#   file.absent:
#    - name: {{ alertmanager.dist_dir}}
#    - require:
#      - alertmanager-remove-symlink

alertmanager-remove-config:
    file.absent:
      - name: {{ alertmanager.config_dir }}
      - require:
        - alertmanager-remove-binary
